package com.wetic.spotify.domain;

/**
 * 
 * @author Dr Strange
 *
 */
public class Song {

	private String name;
	private Integer duration;
	private Artist artist;
	private TypeSong typeSong;

	public Song(String name, Integer duration, Artist artist, TypeSong typeSong) {
		super();
		this.name = name;
		this.duration = duration;
		this.artist = artist;
		this.typeSong = typeSong;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public TypeSong getTypeSong() {
		return typeSong;
	}

	public void setTypeSong(TypeSong typeSong) {
		this.typeSong = typeSong;
	}

}
