package com.wetic.spotify.domain;

import java.util.ArrayList;
import java.util.List;

public class ListSong {
	private String nameList;
	private User user;
	private List<Song> songs = new ArrayList<Song>();
	
	
	public ListSong() {
		super();
	}

	public ListSong(String nameList, User user, List<Song> songs) {
		super();
		this.nameList = nameList;
		this.user = user;
		this.songs = songs;
	}

	public String getNameList() {
		return nameList;
	}

	public void setNameList(String nameList) {
		this.nameList = nameList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	@Override
	public String toString() {
		return "ListSong [nameList=" + nameList + ", user=" + user + ", songs=" + songs + "]";
	}
	
}
