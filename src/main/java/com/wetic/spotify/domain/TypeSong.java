package com.wetic.spotify.domain;

import java.util.List;

public class TypeSong {

	private String name;
	private List<Song> songs;
	
	//Constructeur
	public TypeSong() {
		super();
	}
	
	public TypeSong(String name, List<Song> songs) {
		super();
		this.name = name;
		this.songs = songs;
	}

	//Accesseurs
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	@Override
	public String toString() {
		return "TypeSong [name=" + name + ", songs=" + songs + "]";
	}
	
}
	
	
	