package com.wetic.spotify.domain;

import java.util.ArrayList;

// author : tnicolas

import java.util.List;

public class Artist {
	
	private String name;
	private List<Song> songs = new ArrayList<Song>();
	
	// Constructeur par défaut
	public Artist() {
		super();
	}

	// Constructeur perso
	public Artist(String name, List<Song> songs) {
		super();
		this.name = name;
		this.songs = songs;
	}

	// Accesseurs et mutateurs
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}
	
	// Methodes

}
