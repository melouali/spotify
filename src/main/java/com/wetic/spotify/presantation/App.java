package com.wetic.spotify.presantation;

import java.util.ArrayList;
import java.util.List;

import com.wetic.spotify.domain.Artist;
import com.wetic.spotify.domain.ListSong;
import com.wetic.spotify.domain.Song;
import com.wetic.spotify.domain.TypeSong;
import com.wetic.spotify.domain.User;
import com.wetic.spotify.service.ArtistImpl;
import com.wetic.spotify.service.ListSongimpl;
import com.wetic.spotify.service.SongImpl;
import com.wetic.spotify.service.TypeSongImpl;
import com.wetic.spotify.service.UserImpl;

public class App {

	public static void main(String[] args) {
		// instanciation des classes
		TypeSongImpl typeSongs = new TypeSongImpl();
		Artist artist = new Artist();
		TypeSong typeSong = new TypeSong();
		UserImpl userImpl = new UserImpl();
		SongImpl songImpl = new SongImpl();
		ListSongimpl songsLists = new ListSongimpl();
		ArtistImpl listOfArtists = new ArtistImpl();

		// creation des objets
		List<Song> listOfSongs1 = new ArrayList<Song>();
		List<Song> listOfSongs2 = new ArrayList<Song>();
		List<Song> listOfSongs3 = new ArrayList<Song>();
		TypeSong typeSong1 = new TypeSong("rock", listOfSongs1);
		TypeSong typeSong2 = new TypeSong("metal", listOfSongs2);
		TypeSong typeSong3 = new TypeSong("classic", listOfSongs3);
		List<Song> songs = new ArrayList<Song>();
		List<Song> songs2 = new ArrayList<Song>();
		List<Song> listSong1 = new ArrayList<Song>();
		List<Song> listSongs2 = new ArrayList<Song>();
		List<Song> listSongs3 = new ArrayList<Song>();
		Artist artiste1 = new Artist("U2", listSong1);
		Artist artiste2 = new Artist("Renée la Taupe", listSongs2);
		Artist artiste3 = new Artist("Ray Charles", listSongs3);
		Song song1 = new Song("name1", 1, artiste1, typeSong1);
		Song song2 = new Song("name2", 2, artiste2, typeSong2);
		Song song3 = new Song("name3", 3, artiste3, typeSong3);
		Song song4 = new Song("name4", 4, artiste1, typeSong1);
		listSong1.add(song1);
		listSongs2.add(song2);
		listSongs3.add(song3);
		User user1 = new User("Alex", "Blanchard", "Balex@gmail.com", "Balex170894");
		User user2 = new User("Mamadou", "Zembaboué", "Zemmadou@gmail.com", "petitlionsauvage");
		User user3 = new User("Eddie", "Malou", "Maloueddie@gmail.com", "satelisation");
		ListSong listofSong1 = new ListSong("test", user1, songs);
		ListSong listofSong2 = new ListSong("test", user2, songs2);

		// user test methods
		userImpl.save(user1);
		userImpl.save(user2);
		System.out.println(userImpl.users);
		userImpl.find("Mamadou");
		userImpl.update("Mamadou", user3);
		System.out.println(userImpl.users);
		userImpl.delete(user1);
		System.out.println(userImpl.users);

		// typesong test methods
		typeSongs.saveTypeSong(typeSong1);
		typeSongs.saveTypeSong(typeSong2);
		typeSongs.updateTypeSong(0, typeSong1);
		typeSongs.updateTypeSong(1, typeSong2);
		typeSongs.deleteTypeSong(typeSong1);
		typeSongs.findTypeSongByName(typeSong1);
		System.out.println(typeSongs.toString());
		typeSongs.saveTypeSong(typeSong1);
		typeSongs.saveTypeSong(typeSong2);
		System.out.println(typeSongs.toString());

		// song test methods
		songImpl.save(song1);
		songImpl.save(song2);
		songImpl.save(song3);
		songImpl.save(song4);
		System.out.println("\nAdd");
		displayListSongs(SongImpl.songs);
		System.out.println("\n");
		System.out.println("Find");
		System.out.println(songImpl.find("name2"));
		System.out.println("\nDelete");
		songImpl.delete("name3");
		displayListSongs(SongImpl.songs);
		System.out.println("\nUpdate");
		songImpl.update("name4");
		displayListSongs(SongImpl.songs);

		// list of songs test methods
		songsLists.save(listofSong1);
		System.out.println(songsLists.find(listofSong1.getNameList()).toString());
		songsLists.update(0, listofSong2);
		songsLists.delete(listofSong2);
		System.out.println(songsLists.find(listofSong2.getNameList()));

		// artist test methods
		listOfArtists.saveArtist(artiste1);
		listOfArtists.saveArtist(artiste2);
		System.out.println(listOfArtists.toString());
		listOfArtists.findArtist(artiste2);
		listOfArtists.updateArtist(0, artiste3);
		System.out.println(listOfArtists.toString());
		listOfArtists.deleteArtist(artiste2);
		System.out.println(listOfArtists.toString());

	}

	public static void displayListSongs(List<Song> list) {
		for (Song song : SongImpl.songs) {
			System.out.println("Name: " + song.getName() + ", duration: " + song.getDuration() + "minutes, Artiste: "
					+ song.getArtist().getClass() + ", de type: " + song.getTypeSong().getClass());
		}
	}
}
