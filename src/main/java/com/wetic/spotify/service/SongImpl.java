package com.wetic.spotify.service;

import java.util.ArrayList;
import java.util.List;

import com.wetic.spotify.domain.Artist;
import com.wetic.spotify.domain.Song;
import com.wetic.spotify.domain.TypeSong;

public class SongImpl {
	public static List<Song> songs = new ArrayList<Song>();
	Artist artist = new Artist();
	TypeSong typeSong = new TypeSong();

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	@Override
	public String toString() {
		return "SongImpl [songs=" + songs + ", artist=" + artist + ", typeSong=" + typeSong + "]";
	}

	/**
	 * Method to add the song in the list
	 * 
	 * @param song
	 *            Song entered to add
	 */
	public void save(Song song) {
		songs.add(song);
	}

	/**
	 * Update the song by deleting it and adding a new one
	 * 
	 * @param nameOfSong
	 *            Name of the song to update
	 */
	public void update(String nameOfSong) {
		delete(nameOfSong);
		Song song = new Song("Test update", 5, artist, typeSong);
		songs.add(song);
	}

	/**
	 * Method to delete a song
	 * 
	 * @param nameOfSong
	 *            Name of the song to delete
	 */
	public void delete(String nameOfSong) {
		for (Song song : songs) {
			if (song.getName().equals(nameOfSong)) {
				songs.remove(song);
				return;
			}
		}

	}

	/**
	 * Method to return the song with the name of the song
	 * 
	 * @param nameOfSong
	 *            Name of the song to find
	 * @return Return the song
	 */
	public Song find(String nameOfSong) {
		for (Song song : songs) {
			if (song.getName().equals(nameOfSong)) {
				return song;
			}
		}
		return null;
	}
}