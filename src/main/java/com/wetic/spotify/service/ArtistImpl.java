package com.wetic.spotify.service;

import java.util.ArrayList;
import java.util.List;

// author : tnicolas

import com.wetic.spotify.domain.Artist;

public class ArtistImpl 
 {

	private List<Artist> artists = new ArrayList<Artist>();
	
	public void saveArtist(Artist artiste) {
		artists.add(artiste);
	}
	
	public void updateArtist(int index, Artist artiste) {
		artists.set(index, artiste);
		
	}
	
	public void deleteArtist(Artist artiste) {
		artists.remove(artiste);
	}
	
	public Artist findArtist(Artist artiste) {
		artists.contains(artiste);
		return artiste;
	}

	@Override
	public String toString() {
		return "ArtistImpl [artists=" + artists + "]";
	}


	
}
