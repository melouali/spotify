package com.wetic.spotify.service;

import java.util.ArrayList;
import java.util.List;

import com.wetic.spotify.domain.ListSong;

public class ListSongimpl {
	
	List<ListSong> songsLists = new ArrayList<ListSong>();
	
	public void save(ListSong songsList) {
		songsLists.add(songsList);
	}
	
	public ListSong find(String nameOfList) {
		ListSong result = null;
		for (ListSong listSong : songsLists) {
			if(listSong.getNameList().equals(nameOfList)) {
				result = listSong;
			}
		}
		return result;
	}
	
	public void update(int id ,ListSong songsList) {
		songsLists.set(id, songsList);
	}
	
	public void delete(ListSong songsList) {
		songsLists.remove(songsList);
	}
}
