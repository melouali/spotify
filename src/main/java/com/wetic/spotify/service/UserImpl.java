package com.wetic.spotify.service;

import java.util.ArrayList;
import java.util.List;
import com.wetic.spotify.domain.User;

public class UserImpl {
	public List<User> users = new ArrayList<User>();

	public void save(User user) {
		users.add(user);
	}

	public void update(String firstName, User user1) {
		for (User user : users) {
			if (user.getFirstName().equals(firstName)) {
				users.remove(user);
				users.add(user1);
			}
		}
	}

	public void delete(User user) {
		users.remove(user);
	}

	public List<User> find(String firstname) {
		for (User user1 : users) {
			if (user1.getFirstName().equals(firstname)) {
				return users;				
			}
		}
		return users;
	}
	
}
