package com.wetic.spotify.service;

import java.util.ArrayList;
import java.util.List;

import com.wetic.spotify.domain.TypeSong;

public class TypeSongImpl {

	List<TypeSong> typeSongs = new ArrayList<TypeSong>();
	TypeSong typeSong1 = new TypeSong();

	public void saveTypeSong(TypeSong typeSong1) {
		typeSongs.add(typeSong1);
	}

	public void updateTypeSong(int index, TypeSong typeSong1) {
		typeSongs.set(index, typeSong1);
	}

	public void deleteTypeSong(TypeSong typeSong1) {
		typeSongs.remove(typeSong1);
	}

	public TypeSong findTypeSongByName(TypeSong typeSong1) {
		typeSongs.contains(typeSong1);
		return typeSong1;
	}

	@Override
	public String toString() {
		return "TypeSongImpl [typeSongs=" + typeSongs + ", typeSong1=" + typeSong1 + "]";
	}
	
}
